import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Main extends Component {
    constructor() {
        super();
        //Initialize the state in the constructor
        this.state = { members : {}, };
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {
        /* fetch API in action for 15 members */
        fetch('/member',{
                method: 'POST',
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            })
            .then(response => {
                return response.json();
            })
            .then(members => {
                //Fetched product is stored in the state
                this.setState({ members });
            });
    }

    submitForm (event){
        event.preventDefault();
        const formData = new FormData(event.target);

        fetch('/member',{
                method: 'POST',
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                body: formData,
            })
            .then(response => {
                return response.json();
            })
            .then(members => {
                //Fetched product is stored in the state
                this.setState({ members });
            });
    }

    changePage (link){
        /* fetch API in action for 15 members */
        fetch(link,{
                method: 'POST',
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            })
            .then(response => {
                return response.json();
            })
            .then(members => {
                //Fetched product is stored in the state
                this.setState({ members });
            });
    }

    paginationHtml() {
        return(
            <ul className="pagination">
                { (this.state.members.current_page != 1 ) &&
                    <li className="mx-3">
                        <a onClick={ () => this.changePage(this.state.members.prev_page_url) } href="#">Previous</a>
                    </li>
                }
                { (this.state.members.current_page != this.state.members.last_page) &&
                    <li className="">
                        <a onClick={  () => this.changePage(this.state.members.next_page_url)  } href="#" >Next</a>
                    </li>
                }
            </ul>
        );
    }

    renderMemberFrom() {
        return (
            <form onSubmit={this.submitForm}>
                <div className="form-group">
                    <label htmlFor="firstname">First Name:</label>
                    <input className="form-control" type="text" id="firstname" name="firstname" />
                </div>
                <div className="form-group">
                    <label htmlFor="surname">Last Name:</label>
                    <input className="form-control" type="text" id="surname" name="surname" />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email address:</label>
                    <input className="form-control" type="text" id="email" name="email" />
                </div>
                <div className="form-group">
                    <input className="form-control" type="submit" value="Submit" />
                </div>
            </form>
        );
    }

    renderMemberTable() {
        if(this.state.members.data){
            return this.state.members.data.map(( member, index ) => {
                return (
                    <tr key={member.id} ><th scope="row">{member.id}</th><td>{member.firstname}</td><td>{member.surname}</td><td>{member.email}</td></tr>
                );
            })
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center my-3">
                    <div className="col-md-8">
                        <div className="">
                            { this.renderMemberFrom() }
                        </div>
                    </div>
                </div>

                <div className="row justify-content-center my-3">
                    <div className="col-md-8">
                        <div className="">
                            Graph
                        </div>
                    </div>
                </div>

                <div className="row justify-content-center my-3">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">List</div>

                            <div className="card-body">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">First Name</th>
                                            <th scope="col">Last Name</th>
                                            <th scope="col">Email Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { this.renderMemberTable() }
                                    </tbody>
                                </table>

                                <div>{ this.paginationHtml() }</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(<Main />, document.getElementById('root'));
}
