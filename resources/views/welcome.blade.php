<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pharm Project</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">



        <script src="path/to/chartjs/dist/Chart.js"></script>

    </head>
    <body>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <script>
            var myChart = new Chart(ctx, {...});
        </script>

        <div class="container">
            <div class="row">
                <canvas id="myChart" width="400" height="400"></canvas>
                {{ $report }}
            </div>

            <div class="row">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($result->data as $member)
                            <tr>
                                <th scope="row">{{ $member->id  }}</th>
                                <td>{{ $member->firstname  }}</td>
                                <td>{{ $member->surname  }}</td>
                                <td>{{ $member->email  }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div>
                    @if ($result->last_page > 1)
                    <ul class="pagination">
                        <li class="{{ ($result->current_page == 1) ? ' d-none' : '' }} mx-3">
                            <a href="{{ $result->prev_page_url }}">Previous</a>
                        </li>
                        <li class="{{ ($result->current_page == $result->last_page) ? ' d-none' : '' }}">
                            <a href="{{ $result->next_page_url }}" >Next</a>
                        </li>
                    </ul>
                    @endif
                </div>

            </div>
        </div>
    </body>
</html>
